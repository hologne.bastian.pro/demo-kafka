package fr.bhologne.demokafka.event;

import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class EventHandler {

    @EventListener
    public void handlecustomSpringEventt(CustomSpringEvent customSpringEvent) {
        System.out.println("Handling context started event.");
    }
}
