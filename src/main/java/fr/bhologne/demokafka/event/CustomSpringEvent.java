package fr.bhologne.demokafka.event;

public class CustomSpringEvent  {
    private String message;

    public CustomSpringEvent(String message) {
        this.message = message;
    }
    public String getMessage() {
        return message;
    }
}
