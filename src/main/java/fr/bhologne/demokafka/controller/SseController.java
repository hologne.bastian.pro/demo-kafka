package fr.bhologne.demokafka.controller;


import fr.bhologne.demokafka.event.CustomSpringEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

import java.time.Duration;
import java.time.LocalTime;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@RestController
public class SseController {

    private final ApplicationEventPublisher applicationEventPublisher;

    private final Sinks.Many<ServerSentEvent<String>> sink = Sinks.many().multicast().onBackpressureBuffer();

    public SseController(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }


    @GetMapping("/job")
    public void runJob() {
        Runnable task = () -> job();
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(Runtime.getRuntime().availableProcessors());
        executor.schedule(task, 5, TimeUnit.MILLISECONDS);

    }

    private void job(){

        for (int i = 0; i < 10; i++){

            try {
                System.out.println("Job done");

                sink.tryEmitNext(ServerSentEvent.<String>builder()
                        .event("custom-event")
                        .data("Job done")
                        .build());


                CustomSpringEvent customSpringEvent = new CustomSpringEvent("Job done");
                applicationEventPublisher.publishEvent(customSpringEvent);

                Thread.sleep(10 * 1000); // sleep 10 second
            } catch (InterruptedException ie) {
                Thread.currentThread().interrupt();
            }

        }

    }


    @GetMapping(path = "/stream-sse-periodic")
    public Flux<ServerSentEvent<String>> streamFluxPeriodic() {
        return Flux.interval(Duration.ofSeconds(10))
                .map(sequence -> ServerSentEvent.<String> builder()
                        .id(String.valueOf(sequence))
                        .event("periodic-event")
                        .data("SSE - " + LocalTime.now().toString())
                        .build());
    }

    @GetMapping(path = "/stream-sse")
    public Flux<ServerSentEvent<String>> streamFlux() {
        return sink.asFlux();
    }



}
